package com.antra.raghu.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.raghu.entity.Student;
import com.antra.raghu.repo.StudentRepository;
import com.antra.raghu.service.IStudentService;

@Service
public class StudentServiceImpl implements IStudentService {
	
	@Autowired
	private StudentRepository repo;

	@Override
	public Integer saveStudent(Student sob) {
		sob = repo.save(sob);
		return sob.getStdId();
	}
	
	@Override
	public List<Student> getAllStudents() {
		return repo.findAll();
	}

	@Override
	public void deleteStudent(Integer id) {
		repo.deleteById(id);
	}

}
