package com.antra.raghu.view;

import java.awt.Color;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.antra.raghu.entity.Student;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class StudentPdfView extends AbstractPdfView {

	@Override
	protected void buildPdfDocument(
			Map<String, Object> model, 
			Document document, 
			PdfWriter writer,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		response.addHeader("Content-Disposition", "attachment;filename=students.pdf");
		//create element
		//type,size, style, color
		Font f1 = new Font(Font.TIMES_ROMAN, 32.0f, Font.BOLD, Color.BLUE);
		
		Paragraph p = new Paragraph("WELCOME TO ALL!!",f1);
		p.setAlignment(Element.ALIGN_CENTER);
		p.setSpacingBefore(20.0f);
		p.setSpacingAfter(20.0f);
		
		//add element to document
		document.add(p);
		
		List<Student> list = (List<Student>)model.get("list");
		
		PdfPTable table = new PdfPTable(5);
		table.addCell("Id");
		table.addCell("Name");
		table.addCell("Gender");
		table.addCell("Course");
		table.addCell("Address");
		for(Student s:list) {
			table.addCell(s.getStdId().toString());
			table.addCell(s.getStdName());
			table.addCell(s.getStdGen());
			table.addCell(s.getStdCourse());
			table.addCell(s.getStdAddress());
		}
		
		document.add(table);
	}

}
