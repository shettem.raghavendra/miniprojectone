package com.antra.raghu.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.antra.raghu.entity.Student;

public class StudentExcelView extends AbstractXlsxView {

	@Override
	protected void buildExcelDocument(
			Map<String, Object> model, 
			Workbook workbook, 
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		//to set file name
		response.addHeader("Content-Disposition", "attachment;filename=students.xlsx");
		
		//read data
		List<Student> list = (List<Student>) model.get("list");
		
		//creating a sheet
		Sheet sheet = workbook.createSheet("STUDENTS");
		setHead(sheet);
		setBody(sheet,list);
	}

	private void setBody(Sheet sheet, List<Student> list) {
		int rowNum = 1;
		for(Student s:list) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(s.getStdId());
			row.createCell(1).setCellValue(s.getStdName());
			row.createCell(2).setCellValue(s.getStdGen());
			row.createCell(3).setCellValue(s.getStdCourse());
			row.createCell(4).setCellValue(s.getStdAddress());
		}
	}

	private void setHead(Sheet sheet) {
		Row row = sheet.createRow(0);
		row.createCell(0).setCellValue("ID");
		row.createCell(1).setCellValue("NAME");
		row.createCell(2).setCellValue("GENDER");
		row.createCell(3).setCellValue("COURSE");
		row.createCell(4).setCellValue("ADDRESS");
	}

}
