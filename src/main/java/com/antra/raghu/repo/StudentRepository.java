package com.antra.raghu.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.antra.raghu.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Integer>{

}
