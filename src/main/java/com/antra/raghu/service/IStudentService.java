package com.antra.raghu.service;

import java.util.List;

import com.antra.raghu.entity.Student;

public interface IStudentService {

	public Integer saveStudent(Student sob);
	public List<Student> getAllStudents();
	public void deleteStudent(Integer id);
}
