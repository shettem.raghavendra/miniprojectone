package com.antra.raghu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.antra.raghu.entity.Student;
import com.antra.raghu.service.IStudentService;
import com.antra.raghu.view.StudentExcelView;
import com.antra.raghu.view.StudentPdfView;

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private IStudentService service;
	
	//1. show Register page : /register
	@GetMapping("/register")
	public String showRegister() {
		return "StudentRegister"; //UI PAGE NAME
	}
	
	//2. onsubmit Form, Data is converted into Object
	// Read object using @ModelAttribute
	@PostMapping("/save")
	public String saveData(@ModelAttribute Student student, Model model) 
	{
		Integer id = service.saveStudent(student);
		String message = "Student '"+id+"' created";
		model.addAttribute("message", message);
		return "StudentRegister";
	}
	
	/*
	 * If we enter URL ends with ../all then below controller
	 * method is called. That gets data using service method as List.
	 * This List data is sent to UI using Model Memory.
	 * Here UI page name is: StudentData
	 * 
	 */
	@GetMapping("/all")
	public String showData(
			@RequestParam(value = "message", required = false) String message,
			Model model) 
	{
		List<Student> list = service.getAllStudents();
		model.addAttribute("list", list);
		model.addAttribute("message", message);
		return "StudentData";
	}
	/***
	 * On click DELETE HyperLink read ID as Request Parameter
	 * 
	 */
	@GetMapping("/delete")
	public String delete(
			@RequestParam("id") Integer id,
			RedirectAttributes attributes
			)
	{
		service.deleteStudent(id);
		attributes.addAttribute("message", id+" IS DELETED");
		return "redirect:all"; //../all?message=_________
	}
	
	/***
	 * Show data in excel file
	 */
	@GetMapping("/excel")
	public ModelAndView showExcelData() {
		ModelAndView m = new ModelAndView();
		m.setView(new StudentExcelView());
		
		List<Student> list = service.getAllStudents();
		m.addObject("list", list);
		return  m;
	}
	
	/***
	 * Show data in PDF file
	 */
	@GetMapping("/pdf")
	public ModelAndView showPdfData() {
		ModelAndView m = new ModelAndView();
		m.setView(new StudentPdfView());
		
		List<Student> list = service.getAllStudents();
		m.addObject("list", list);
		return  m;
	}
	
}
