package com.antra.raghu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="student_tab")
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="std_id_col")
	private Integer stdId;
	
	@Column(name="std_name_col")
	private String stdName;
	
	@Column(name="std_gen_col")
	private String stdGen;
	
	@Column(name="std_course_col")
	private String stdCourse;
	
	@Column(name="std_addr_col")
	private String stdAddress;
	
}
