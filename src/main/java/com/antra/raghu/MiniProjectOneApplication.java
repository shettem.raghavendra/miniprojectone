package com.antra.raghu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectOneApplication.class, args);
	}

}
