<html>
  <head>
    <meta charset="UTF-8" />
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
    />
  </head>
  <body>
    <div class="container">
      <div class="card">
        <div class="card-header bg-primary text-center text-white">
          <h3>WELCOME TO STUDENT REGISTER PAGE</h3>
        </div>
        <div class="card-body">
          <form method="post" action="save" id="stdForm">
            NAME
            <input
              type="text"
              name="stdName"
              id="stdName"
              class="form-control"
            />
            <div id="stdNameError"></div>
            GENDER
            <input type="radio" name="stdGen" value="Male" />Male
            <input type="radio" name="stdGen" value="Female" />Female
            <div id="stdGenError"></div>
            <br />
            Courses
            <select name="stdCourse" id="stdCourse" class="form-control">
              <option value="">--SELECT--</option>
              <option value="CORE JAVA">CORE JAVA</option>
              <option value="ADV JAVA">ADV JAVA</option>
              <option value="SPRING">SPRING</option>
            </select>
            <div id="stdCourseError"></div>
            ADDRESS
            <textarea
              name="stdAddress"
              id="stdAddress"
              class="form-control"
            ></textarea>
            <div id="stdAddressError"></div>
            <button type="submit" class="btn btn-success">CREATE</button>
          </form>
        </div>
        <!-- card body end -->
        <div class="card-footer bg-info text-white">${message}</div>
        <!-- card footer end -->
      </div>
    </div>
    <script type="text/javascript">
      $(document).ready(function () {
        $("#stdNameError").hide();
        $("#stdGenError").hide();

        var stdNameError = false;
        var stdGenError = false;


        function validate_stdName() {
          var val = $("#stdName").val();
          if (val == "") {
            $("#stdNameError").show();
            $("#stdNameError").css("color", "red");
            $("#stdNameError").html("Enter Student Name");
            stdNameError = false;
          } else {
            $("#stdNameError").hide();
            stdNameError = true;
          }
          return stdNameError;
        }

        function validate_stdGen() {
          var len = $('input[type="radio"][name="stdGen"]:checked').length;
          if (len == 0) {
            $("#stdGenError").show();
            $("#stdGenError").css("color", "red");
            $("#stdGenError").html("Select Gender");
            stdGenError = false;
          } else {
            $("#stdGenError").hide();
            stdGenError = true;
          }
          return stdGenError;
        }

        $("#stdName").keyup(function () {
          validate_stdName();
        });
        $('input[type="radio"][name="stdGen"]').change(function () {
          validate_stdGen();
        });

        $("#stdForm").submit(function () {
          validate_stdName();
          validate_stdGen();
          if(stdNameError && stdGenError)  return true
          else return false;
        })
      });
    </script>
  </body>
</html>
