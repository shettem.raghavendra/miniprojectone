<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <html>

    <head>
        <meta charset="UTF-8" />
        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    </head>

    <body>
        <div class="container">

            <div class="card">
                <div class="card-header bg-primary text-center text-white">
                    <h3>WELCOME TO STUDENT DATA PAGE</h3>
                </div>
                <div class="card-body">
                    <a href="excel">Excel Export</a> | <a href="pdf">PDF Export</a>
                    <table class="table">
                        <tr class="bg-info text-white">
                            <th>ID</th>
                            <th>NAME</th>
                            <th>GENDER</th>
                            <th>COURSE</th>
                            <th>ADDRESS</th>
                            <th>OPERATIONS</th>
                        </tr>
                        <!-- List<Student> list = ...
                            for(Student ob:list) {} -->
                        <c:forEach items="${list}" var="ob">
                            <tr>
                                <td>${ob.stdId}</td>
                                <td>${ob.stdName}</td>
                                <td>${ob.stdGen}</td>
                                <td>${ob.stdCourse}</td>
                                <td>${ob.stdAddress}</td>
                                <td>
                                    <a href="delete?id=${ob.stdId}" class="btn btn-danger">DELETE</a>
                                    <a href="#" class="btn btn-warning">EDIT</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="card-footer bg-white text-center text-danger">
                    <span>${message}</span>
                </div>
            </div>
        </div>
    </body>

    </html>